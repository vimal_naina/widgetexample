package com.vimalnaina.widgetexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtName = findViewById<EditText>(R.id.etxtName)
        val txtEmail = findViewById<EditText>(R.id.etxtEmail)
        val txtPhone = findViewById<EditText>(R.id.etxtPhone)
        val submit = findViewById<Button>(R.id.btnSubmit)

        submit.setOnClickListener(){
            val name = txtName.text.toString()
            val email = txtEmail.text.toString()
            val phone = txtPhone.text

            Toast.makeText(this,"Name: ${name}\n" + "Email: ${email}\n" + "Phone: ${phone}",Toast.LENGTH_LONG).show()

        }
    }
}